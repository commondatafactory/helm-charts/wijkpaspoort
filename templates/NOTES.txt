===
Wijkpaspoort can be accessed via port 8080 on the following DNS name from within your cluster:
{{ template "wijkpaspoort.fullname" . }}.{{ .Release.Namespace }}.svc.cluster.local
--
{{ if .Values.wijkpaspoort.ingress.enabled -}}
From outside the cluster, the server URL(s) are:
{{ range .Values.wijkpaspoort.ingress.hosts }}
https://{{ . }}
{{- end }}
{{- end }}
